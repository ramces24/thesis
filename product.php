<?php
 	include('includes/header.php');
	include('includes/sidebar.php');
 

?>
	<!-- Start: Content -->
        <section id="content_wrapper">
            <section id="content">
            	<!-- content -->
                <div class="row animated-delay" data-animate='["500","fadeIn"]'>
                      	<div id="animation-switcher" class="ph20">
                             <div class="col-md-6" style = "text-align:left">
                             <label class = "control-label" style = "font-size:30px;padding:12px 16px 0 0"><strong>Products</strong></label>
                                    <button style = "margin-bottom:20px;" class="btn btn-warning" data-effect="mfp-flipInY" ><strong>New Product</strong></button>
                              </div> 
                            <div class = "row">
                                <div id = "datatable2_wrapper" class = "dataTables_wrapper form-inline dt-bootstrap no-footer">
                                    <table class="table table-striped table-hover dataTable no-footer" id="datatable2" cellspacing="0" width="100%" role="grid" aria-describedby="datatable2_info" style="width: 100%;">
                                        <thead>
                                          <tr role="row">
                                          <th class="sorting_asc" tabindex="0" aria-controls="datatable2" rowspan="1" colspan="1" aria-label="Name: activate to sort column descending" style="width: 122px;" aria-sort="ascending">Id</th>
                                          <th class="sorting_asc" tabindex="0" aria-controls="datatable2" rowspan="1" colspan="1" aria-label="Name: activate to sort column descending" style="width: 122px;" aria-sort="ascending">Name</th>
                                          </tr>
                                        </thead>

                                        <tbody class="categories">
                                            <script type="text/javascript">
                                                    $(document).ready(function(){
                                                        retrieve_category();
                                                    });
                                            </script>
                                            <tr class="odd"><td valign="top" colspan="6" class="dataTables_empty">No matching records found</td></tr>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                      </div>
                </div>
             	<!-- end content -->
                <!-- Show Modal-->
                <div id="modal-form" class="width1 admin-form mfp-with-anim mfp-hide">
                    <div class="panel">
                        <div class="panel-heading">
                            <span class="panel-title"><i class="fa fa-rocket"></i>New Product</span>
                        </div>
                        <form method="post" action="" id="form" class = "form-horizontal" enctype="multipart/form-data">
                            <div class="panel-body p25">                                 
                                <div class="section">    
                                    <div class = "section row">
                                        <div class = "col-md-12">      
                                                <input type="text" name="category_name" id="product" class="gui-input" placeholder="Product Code"/>
                                        </div>
                                    </div>
                                    <div class = "section row">    
                                        <div class = "col-md-12">       
                                                <input type="text" name="" id="product" class="gui-input" placeholder="Product Name"/>
                                        </div>
                                    </div>
                                    <div class = "section row">    
                                        <div class = "col-md-10">         
                                                <select class="form-control category">
                                                    <option>Category</option>
                                                </select>
                                        </div>
                                        <div class = "col-xs-2">         
                                                <button class ="btn  show_category"><strong style = "color:#fff">New Category</strong></button>
                                        </div>
                                    </div>
                                    <div class = "section row  cat_input" style = "display:none">
                                        <div class = "col-md-9">         
                                                <input type="text" name="" id="product" class="gui-input" placeholder="New Category" />
                                        </div>
                                        <div class = "col-md-3">         
                                                <button class ="btn btn-success cat_cancel"><strong style = "color:#fff">Create</strong></button>
                                                <button class ="btn btn-danger cat_cancel"><strong style = "color:#fff">Cancel</strong></button>
                                        </div>
                                     
                                    </div>
                                    <div class = "section row">    
                                        <div class = "col-md-10">         
                                                <select class="form-control brand">
                                                    <option>Brand</option>
                                                </select>
                                        </div>
                                        <div class = "col-md-2">         
                                                <button class ="btn show_brand"><strong style = "color:#fff">New Brand</strong></button>
                                        </div>
                                    </div>
                                    <div class = "section row brand_input" style = "display:none">
                                        <div class = "col-md-9">         
                                                <input type="text" name="" id="product" class="gui-input" placeholder="New Brand" />
                                        </div>
                                        <div class = "col-md-3">     
                                        <button class ="btn btn-success cat_cancel"><strong style = "color:#fff">Create</strong></button>    
                                                <button class ="btn btn-danger brand_cancel"><strong style = "color:#fff">Cancel</strong></button>
                                        </div>
                                    </div> 
                                    <div class = "section row"  style = "text-align:right">    
                                        <div class = "col-md-2">       
                                               <label class = "control-label"><strong>Product price :</strong></label>
                                        </div>
                                        <div class="col-md-4">
                                            <div class = "input-group">
                                                  <input class="form-control" type="number" value = "100" min = "10">
                                                  <span class="input-group-addon">
                                                    php
                                                  </span>
                                            </div>  
                                        </div>
                                        <div class = "col-md-2"  style = "text-align:right">       
                                               <label class = "control-label"><strong>Number of stocks :</strong></label>
                                        </div>
                                        <div class = "col-md-4">
                                                <input type = "number" value = "1" class = "form-control" min = "0">
                                        </div>
                                    </div>
                                   <div class="section row"  style = "text-align:right">
                                        <div class = "col-md-3">
                                            <div class="checkbox-custom fill checkbox-disabled" style = "margin-top:12px;">
                                                        <input type="checkbox" id="checkboxDefault11" class = "check_sale">
                                                        <label for="checkboxDefault11"><strong>Enable/Disable Sale</strong></label>
                                            </div>
                                        </div>
                                        <div class = "col-md-2" >
                                                <label class="control-label" for="daterangepicker1" ><strong>Sale Price :</strong></label>
                                         </div>
                                         <div class = "col-md-2">
                                                <input type = "number" value = "1" class = "form-control price_sale" min = "0">
                                         </div>
                                         <div class="col-md-2">
                                                <label class="control-label" for="daterangepicker1"><strong>Sale Date :</strong></label>
                                         </div>       
                                        <div class="col-md-3">
                                            <input type="text" class="form-control pull-right date_sale" name="daterange" id="daterange1">
                                        </div>
                                    </div> 
                                    <div class = "section row">
                                        <div class = "col-md-2">
                                            <img src="img/default.jpg" alt = "product_img" width="100%" height="90" id = "product_image" class=" pull-right">
                                        </div>
                                        <div class = "col-md-7" style = "margin-top:10px;">
                                            <input type="text"  class="gui-input product" placeholder="New Brand" style = "float:right" />
                                        </div>
                                         <div class = "col-md-3" style = "margin-top:12px;">
                                             <input type = "file" name = "file" value = "test" id = "product_url" />
                                             <script type="text/javascript">
                                                 $('#product_url').change(function(){
                                                   readURL(this);
                                                 })
                                             </script>
                                        </div>
                                    </div>                           
                                </div>
                            </div>
                            <div class="panel-footer">
                                    <button type = "submit"  class="exit btn btn-success mb10 mr5 notification" data-note-style="success" data-note-shadow="true" onClick="new_product()">Publish</button>
                                   

                            </div>
                            
                        </form>
                <!-- End Modal -->
            </section>
        </section>
        <script type="text/javascript" src = "assets/js/ajax/ajax_helper.js"></script>
        <script type="text/javascript">
        jQuery(document).ready(function() {
            "use strict";
            // Core.init();

            
            $('#animation-switcher button').on('click', function() {
                $('#animation-switcher').find('button').removeClass('active-animation');
                $(this).addClass('active-animation item-checked');
                // Inline Admin-Form example 
                $.magnificPopup.open({
                    removalDelay: 500, //delay removal by X to allow out-animation,
                    items: {
                        src: "#modal-form"//findActive()
                    },
                    // overflowY: 'hidden', // 
                    callbacks: {
                        beforeOpen: function(e) {
                            var Animation = $("#animation-switcher").find('.active-animation').attr('data-effect');
                            // alert(Animation);
                            this.st.mainClass = Animation;
                        }
                    },
                    midClick: true // allow opening popup on middle mouse click. Always set it to true if you don't provide alternative source.
                });
               
            }); 
             $('#daterange1').daterangepicker();
            $('.exit').on('click',function(){
                $.magnificPopup.close();
            })
            $('.show_category').click(function(){
                $('.category').attr('disabled','disabled')
                $('.cat_input').show();
                $('.cat_cancel').show();
            })
            $('.show_brand').click(function(){
                $('.brand').attr('disabled','disabled')
                $('.brand_input').show();
                $('.brand_cancel').show();
            })
            $('.cat_cancel').click(function(){
                $(this).hide();
                $('.cat_input').val('');
                $('.cat_input').hide();
                 $('.category').attr('disabled',false)
            });
             $('.brand_cancel').click(function(){
                $(this).hide();
                $('.brand_input').val('');
                $('.brand_input').hide();
                 $('.brand').attr('disabled',false)
            });
             $('.show-calendar').click(function(){
                var dash = "-"
                $('#daterange1').val($('input[name="daterangepicker_start"]').val().concat(dash,$('input[name="daterangepicker_end"]').val()));
             })
             $('.check_sale').change(function(){
                 if(this.checked){
                    $('.date_sale').attr('disabled','disabled');
                    $('.price_sale').attr('disabled','disabled');
                 } 
                 else{
                    $('.date_sale').attr('disabled',false);
                    $('.price_sale').attr('disabled',false);
                 }
              });   

            // PNotify Plugin Event Init
            
        });
    </script>
  
<?php include('includes/footer.php'); ?>
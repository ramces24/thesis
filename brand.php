<?php
 	include('includes/header.php');
	include('includes/sidebar.php');

?>
	<!-- Start: Content -->
        <section id="content_wrapper">
            <section id="content">
            	<!-- content -->
                <div class="row animated-delay" data-animate='["500","fadeIn"]'>
                      	<div id="animation-switcher" class="ph20">
                              <div class="col-md-6" style = "text-align:left">
                             <label class = "control-label" style = "font-size:30px;padding:12px 16px 0 0"><strong>Brands</strong></label>
                                    <button style = "margin-bottom:20px;" class="btn btn-warning" data-effect="mfp-flipInY" ><strong>New Brand</strong></button>
                              </div> 
                        </div>
                </div>
             	<!-- end content -->
                <!-- Show Modal-->
                <div id="modal-form" class="popup-basic admin-form mfp-with-anim mfp-hide">
                    <div class="panel">
                        <div class="panel-heading">
                            <span class="panel-title"><i class="fa fa-rocket"></i>New Brand</span>
                        </div>
                        <form method="post" action="" id="comment">
                            <div class="panel-body p25">                                 
                                <div class="section row">
                                            <div class="alert alert-danger alert-dismissable" style = "display:none">
                                                  <i class="fa fa-remove pr10"></i>
                                                  <strong>Required!</strong>
                                            </div>  
                                            <input type="text" name="category_name" id="field_name" class="gui-input" placeholder="Category Name">
                                </div>
                            </div>
                            <div class="panel-footer">
                                     <button type = "submit"  onClick = "new_brand_category('brand')" class="btn btn-success mb10 mr5 notification" data-note-style="success" data-note-shadow="true">Publish</button>
                            </div>
                            
                        </form>
                <!-- End Modal -->
            </section>
        </section>
         <script type="text/javascript" src = "assets/js/ajax/ajax_helper.js"></script>
        <script type="text/javascript" src = "assets/js/customized.js"></script>
<?php include('includes/footer.php'); ?>
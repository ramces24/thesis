
//--------insert helper--------------

// insert brand or category
function new_brand_category(entity){
	var field_name = $('#field_name').val();
	if(field_name != ""){

		$.ajax({
			url:'includes/ajaxquery/insert_brand&category.php',
			type:'post',
			data:{category_name:field_name,entity_name:entity},
			success:function(html){
				alert('okay');
				$.magnificPopup.close();
				$('.alert-dismissable').css("display","none");
				$('#field_name').val('');
				notification_display(field_name+" ",'Added !');

			}
		});

		
		
	}
	else{

		$('.alert-dismissable').css("display","block");
	}

	
}
function new_product(){
	$('form#form').on('submit',function(){
			var  formData = new FormData(this);
			$.ajax({
				url:'includes/ajaxquery/insert_product.php',
				type: "POST",
				data:formData,
				contentType: false,
				cache: false,
				processData:false,
				success:function(html){
					notification_display('product','added');
				}
			})

	})

			
}
function readURL(input) {
    var url = input.value;
    var ext = url.substring(url.lastIndexOf('.') + 1).toLowerCase();
    if (input.files && input.files[0]&& (ext == "gif" || ext == "png" || ext == "jpeg" || ext == "jpg")) {
        var reader = new FileReader();

        reader.onload = function (e) {
            $('#product_image').attr('src', e.target.result);
        }
      	filename_container();
        reader.readAsDataURL(input.files[0]);
    }else{
         alert('File type is not supported');
    }

}
function filename_container(){
	  		var files = $('#product_url').prop("files");
	        var names = $.map(files, function(val) { return val.name; });
	     	$('.product').val(names);
	        
}

//end insert brand or category

//--------end of insert helper--------------
//--------retrieve helper-------------------

//get all category
function retrieve_category(){
	$.ajax({
			url:'includes/ajaxquery/get_category.php',
			success:function(categories_holder){
				$('.categories').html(categories_holder);
			}

	});
}
//end of get all category

//--------end of retrieve helper------------ 
function notification_display(event_title,desc){
				var Stacks = {
                stack_top_right: {
                    "dir1": "down",
                    "dir2": "left",
                    "push": "top",
                    "spacing1": 10,
                    "spacing2": 10
                },
                stack_top_left: {
                    "dir1": "down",
                    "dir2": "right",
                    "push": "top",
                    "spacing1": 10,
                    "spacing2": 10
                },
                stack_bottom_left: {
                    "dir1": "right",
                    "dir2": "up",
                    "push": "top",
                    "spacing1": 10,
                    "spacing2": 10
                },
                stack_bottom_right: {
                    "dir1": "left",
                    "dir2": "up",
                    "push": "top",
                    "spacing1": 10,
                    "spacing2": 10
                },
                stack_bar_top: {
                    "dir1": "down",
                    "dir2": "right",
                    "push": "top",
                    "spacing1": 0,
                    "spacing2": 0
                },
                stack_bar_bottom: {
                    "dir1": "up",
                    "dir2": "right",
                    "spacing1": 0,
                    "spacing2": 0
                },
                stack_context: {
                    "dir1": "down",
                    "dir2": "left",
                    "context": $("#stack-context")
                },
            }

                
                var noteStyle = $('.notification').data('note-style');
                var noteShadow = $('.notification').data('note-shadow');
                var noteOpacity = $('.notification').data('note-opacity');
                var noteStack = $('.notification').data('note-stack');
                var width = "290px";

                var noteStack = noteStack ? noteStack : "stack_top_right";
                var noteOpacity = noteOpacity ? noteOpacity : "1";

                function findWidth() {
                    if (noteStack == "stack_bar_top") {
                        return "100%";
                    }
                    if (noteStack == "stack_bar_bottom") {
                        return "70%";
                    } else {
                        return "290px";
                    }
                }

                new PNotify({
                    title: event_title.concat(desc),
                    text: 'Admin',
                    shadow: noteShadow,
                    opacity: noteOpacity,
                    addclass: noteStack,
                    type: noteStyle,
                    stack: Stacks[noteStack],
                    width: findWidth(),
                    delay: 1400
                });

           
              

}
 jQuery(document).ready(function() {
            "use strict";
            $('#animation-switcher button').on('click', function() {
                $('#animation-switcher').find('button').removeClass('active-animation');
                $(this).addClass('active-animation item-checked');
                // Inline Admin-Form example 
                $.magnificPopup.open({
                    removalDelay: 500, //delay removal by X to allow out-animation,
                    items: {
                        src: "#modal-form"//findActive()
                    },
                    // overflowY: 'hidden', // 
                    callbacks: {
                        beforeOpen: function(e) {
                            var Animation = $("#animation-switcher").find('.active-animation').attr('data-effect');
                            // alert(Animation);
                            this.st.mainClass = Animation;
                        }
                    },
                    midClick: true // allow opening popup on middle mouse click. Always set it to true if you don't provide alternative source.
                });
                $('.alert-dismissable').css("display","none");
                $('#field_name').val('');
            }); 
           
});